// Apuluokka informaation palauttamiseksi ProcessLocations -luokasta
// Juho Uusi-Luomalahti		21.8.2014

package fi.vv.geoaita;

import android.location.Location;

public class AnalyzedData {
	private Location loc;
	private boolean isInsideGeofence;
	private double dist2nearest;
	private double locFixSpeed;
	private String geofenceName;
	
	public AnalyzedData(Location loc, boolean isInsideGoefence, String geofenceName, double dist2nearest, 
			double locFixSpeed ){
		this.loc = loc;
		this.isInsideGeofence = isInsideGoefence;
		this.dist2nearest = dist2nearest;
		this.locFixSpeed = locFixSpeed;
		this.geofenceName = geofenceName;
	}
	
	public Location getLoc() {
		return loc;
	}
	
	public String getGeofenceName(){
		return geofenceName;
	}
	
	public double getLocFixSpeed(){
		return locFixSpeed;
	}
	
	public double getDist2nearest(){
		return dist2nearest;
	}

	public boolean isInsideGeofence(){
		return isInsideGeofence;
	}
	
	
}
