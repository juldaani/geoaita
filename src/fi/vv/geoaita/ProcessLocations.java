// Luokka sisältää koodin gps-datan prosessoimiseksi
// ( analysoi onko mobiililaite geofencen sisäpuolella)
//  Juho Uusi-Luomalahti			21.8.2014


package fi.vv.geoaita;

import java.util.Collections;
import java.util.LinkedList;
import java.util.PriorityQueue;

import android.location.Location;

public class ProcessLocations {
	
	private LinkedList<Location> locationList;//, filteredLocList;		// List of previous locations
	private PriorityQueue<Double> speedList, nearestGeofenceList;
	private double dist2nearest;
	
	
	// *****************************
	// Konstruktori
	// ******************************
	public ProcessLocations(){
		locationList = new LinkedList<Location>();
		speedList =  new PriorityQueue<Double>( 3, Collections.reverseOrder() );
		nearestGeofenceList =  new PriorityQueue<Double>();
	}
	
	
	// *********************************
	// Interface for accessing to the class
	// ********************************
	public AnalyzedData inputLocation(Location location, LinkedList<Geofence> geofenceList){
		
		// If locationList IS NOT full, then add location to the first position
		if( (location != null) && (locationList.size() < Parameters.PREV_LOCATIONS_LIST_LEN) ){
			locationList.addFirst(location);
		}
		// If locationList IS full, then add to first and remove last
		else if( (location != null) ){
			locationList.addFirst(location);
			locationList.removeLast();
		}
		
		if(locationList.size() < 2){
			return null;
		}
		
		// Define speed for location fix
		double locFixSpeed = getSpeedForLocFix( locationList.getFirst() );
		
		// Find out if device is stopped inside some geofence
		Geofence curGeof = analyzeIsInsideGeofence(locationList.getFirst(), geofenceList);
		
		AnalyzedData analyzedLocData = new AnalyzedData(locationList.getFirst(), false, "", 
				dist2nearest, locFixSpeed);
		
		// If inside some geofence then put information about the geofence to the analyzedLocData
		if(curGeof != null){
			analyzedLocData = new AnalyzedData(locationList.getFirst(), true, curGeof.getPlace(),
					dist2nearest, locFixSpeed);
		}
		
		// Return analyzed data
		return analyzedLocData;
	}
	
	
	
	//***********************************
	// Define speed for location fix
	//
	// Speeds are saved to priority queue and the fastest speed is always returned from the queue.
	// Speeds are filtered this way because if the current speed would always be returned, then fast changes in speed
	// would cause problems in defining location update interval. Reason for that is the location 
	// update interval calculation. Location update interval calculation is based on the estimated 
	// time for arriving insinde nearest geofence. For example when driving car and stop for couple of minutes, then
	// without filtering program expects that mobile device is moving with walking speed. If then 
	// driver starts again to drive forward, then program may miss some geofence, because program expects 
	// walking speed and it has adjusted location update interval according to that speed.
	//************************************	
	private double getSpeedForLocFix(Location curLoc){
		double tmpSpeed, fastestSpeed=0.0;
		
		// If no speed information available for location fix, then calculate speed based on coordinates
		if( curLoc.hasSpeed() == false){
			Location prevLoc = findNthSample(1, locationList);
			
			long timePrev = prevLoc.getTime();
			long timeCur = curLoc.getTime();
			long timeDelta = (timeCur - timePrev)/1000000000;	// [s]
			
			double distDelta = curLoc.distanceTo(prevLoc);
			tmpSpeed = distDelta / timeDelta;
			
		}
		// If there is speed information, then use it
		else{
			tmpSpeed = curLoc.getSpeed();
		}
		
		// If speedsList IS NOT full, then add location
		if( (speedList != null) && (speedList.size() < 3) ){
			speedList.add(tmpSpeed);
			fastestSpeed = speedList.peek();
		}
		// If speedList IS full, then add new and poll the largest speed value
		else if( (speedList != null) ){
			speedList.add(tmpSpeed);
			fastestSpeed = speedList.poll();
		}
		
		return fastestSpeed;
	}
	
	
	//*********************************************
	// Method checks if device is inside some geofence
	// Calculates also dist to nearest geofence
	//**********************************************
	// TODO 
	// handle multiple crossing geofences
	// handle polygons
	// consider accuracy
	private Geofence analyzeIsInsideGeofence(Location curLoc, LinkedList<Geofence> geofenceList){
		for (Geofence geof : geofenceList) {	//iterate through list
			// Create new Location objects because distanceTo method requires it as an input
			Location tmpLocation = new Location("tmpLoc");
			tmpLocation.setLatitude(geof.getLatLng().latitude);
			tmpLocation.setLongitude(geof.getLatLng().longitude);
			
			int geofRad = geof.getRadius();
			double dist2GeofCenterpoint = curLoc.distanceTo(tmpLocation);
			
			double dist2GeofEdge = dist2GeofCenterpoint - geofRad;
			nearestGeofenceList.add(dist2GeofEdge);
			
			// If the distance between device's current location and geofence's centerpoint  
			// is less than the geofence's radius, then device is inside the geofence
			if( (dist2GeofCenterpoint) < geofRad){
				nearestGeofenceList.poll();		// remove distance to geofence which inside we are
				dist2nearest = nearestGeofenceList.peek();	// get second closest dist
				nearestGeofenceList.clear();
				return geof;
			}
		}
		
		dist2nearest = nearestGeofenceList.peek();
		nearestGeofenceList.clear();
		
		// return null if device is not inside any geofence
		return null;
	}
	
	
	// *************************************
	// To find nth sample in a list
	// index is 0 the first, 1 second, 2 third....
	// *************************************
	Location nthLocation;
	
	private Location findNthSample(int n, LinkedList<Location> locList){
		nthLocation = null;
		int i = 0;
		for (Location loc : locList) {		//iterate through the locationList
			if(i == n){
				nthLocation = loc;
			}
			i++;
		}
		return nthLocation;
	}
	
	
}
