// N�ytt�� kuljetun reitin kartalla
// TTY, Porin laitos 			21.8.2014

package fi.vv.geoaita;

import java.util.LinkedList;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import fi.tut.pori.ajologgeri.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class MapActivity extends Activity implements OnInfoWindowClickListener {
	
    private GoogleMap map;
    private boolean mapFirstCreate;
    
    private CircleOptions circleOptions, clickedCircleOptions;
    private Circle circle, center, clickedCircle;
    
    private LinkedList<Geofence> geofenceList;	// List for geofences
    private Marker clickedGeofenceMarker;
    
    private LatLng curGeofenceLatLng;
    private int curGeofenceRad;
    
    GeofenceReaderWriter geofences;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        
        mapFirstCreate = true;
        
        clickedCircle = null;
        circle = null;
        
        geofences = new GeofenceReaderWriter(getApplicationContext() );
        
        geofenceList = geofences.getGeofenceList();
        
        // Load a map
        try {
        	initMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    
    //************************************
    // Set listeners on a map
    //************************************
    private void setMapListeners(){
    	// Listener for long click on a map
        // Creates a circle around the clicked point
        map.setOnMapLongClickListener(new OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng latLng) {
            	EditText radEditText = (EditText)findViewById(R.id.sade_editText);
            	curGeofenceRad = -1;
            	boolean isRadInput = false;
            	
            	curGeofenceLatLng = latLng;
            	
            	// if radius edittext has input, then put it to curGeofenceRad
            	if( !radEditText.getText().toString().equals("") ){
            		curGeofenceRad = Integer.parseInt( radEditText.getText().toString() );
                	isRadInput = true;
            	}
            
            	// If circle does not exist then create object
                if( (circle == null) && isRadInput){
	        		circleOptions = new CircleOptions()
		    			.center(latLng)
		    			.radius(curGeofenceRad)
		    			.strokeWidth(4.0f)
		    			.strokeColor(Color.BLUE);
	        		circle = map.addCircle(circleOptions);
	        		
	          		circleOptions = new CircleOptions()
		    			.center(latLng)
		    			.radius(1)
		    			.strokeWidth(15.0f)
		    			.strokeColor(Color.RED); 
	        		center = map.addCircle(circleOptions);
	        		
                }
                // If circle already exists then just change settings
                else if(isRadInput){
                	circle.setCenter(latLng);
                	circle.setRadius(curGeofenceRad);
                	center.setCenter(latLng);
                }
                // If no radius
                else{
                	Toast.makeText(getApplicationContext(), "Sy�t� s�de!", Toast.LENGTH_SHORT).show();
                	curGeofenceLatLng = null;
                }
                
            }
        });
        
        // Listener for click on a map
        // Removes circles
        map.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng latLng) {
				nullifyGraphics();
			}
        });
        
        // Listener for marker click event
        // Creates a circle around the marker
        map.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {
				
				// Find clicked object from the geofenceList
				clickedGeofenceMarker = marker;
				
				Geofence clickedGeof = geofences.findGeofence(clickedGeofenceMarker.getPosition(),
						clickedGeofenceMarker.getTitle() );
				
				// If circle does not exist then create object
                if( clickedCircle == null ){
                	clickedCircleOptions = new CircleOptions()
		    			.center(clickedGeof.getLatLng())
		    			.radius(clickedGeof.getRadius())
		    			.strokeWidth(4.0f)
		    			.strokeColor(Color.BLUE);
                	clickedCircle = map.addCircle(clickedCircleOptions);
	        		
                }
                // If circle already exists then just change settings
                else{
                	clickedCircle.setCenter(clickedGeof.getLatLng());
                	clickedCircle.setRadius(clickedGeof.getRadius());
                }
				
				return false;
			}
		
        });
    }
    
    
    //*******************************************
    // Remove graphcis
    //******************************************
    private void nullifyGraphics(){
    	if(clickedCircle != null){
			clickedCircle.remove();
		}
		clickedCircle = null;
		
		if(circle != null){
			circle.remove();
		}
		circle = null;
		
		if(center != null){
			center.remove();
		}
		center = null;
		
		clickedGeofenceMarker = null;
    }
    
    
    //******************************
    // Add geofence button
    //******************************
    public void addGeofenceButtonClick(View view){
    	EditText paikkaEditText = (EditText) findViewById(R.id.paikka_editText);
    	String paikka = paikkaEditText.getText().toString();
    	EditText tapahtumaEditText = (EditText) findViewById(R.id.tapahtuma_editText);
    	String tapahtuma = tapahtumaEditText.getText().toString();
    	EditText sadeEditText = (EditText) findViewById(R.id.sade_editText);
    	
    	if( (curGeofenceLatLng != null) && (curGeofenceRad > 0) ){
    		MarkerOptions markerOptions = new MarkerOptions()
		     	.position(curGeofenceLatLng)
		     	.title(paikka)
		     	.snippet(tapahtuma +", "+ curGeofenceRad + " m");
    			
    		map.addMarker(markerOptions);
    		
    		Geofence newGeofence = new Geofence(curGeofenceLatLng, tapahtuma, paikka, curGeofenceRad);
    		// Call GpsLocationService to add the geofence
    		geofences.addGeofence(getApplicationContext(), newGeofence);	
    		
    		// Set edit texts to empty
    		tapahtumaEditText.setText("");
    		paikkaEditText.setText("");
    		sadeEditText.setText("");
    		
    		curGeofenceLatLng = null;
    		
    		nullifyGraphics();
    		
    		// Hide keyboard
    		InputMethodManager inputMgr = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
    		inputMgr.hideSoftInputFromWindow(tapahtumaEditText.getWindowToken(), 0);
    	}
    	else{
    		Toast.makeText(getApplicationContext(), "Valitse sijainti!", Toast.LENGTH_SHORT).show();
    	}
    	
    }
    
    
    //******************************
    // Remove geofence button
    //******************************
    public void removeGeofenceButtonClick(View view){
    	if( clickedGeofenceMarker != null){
    		// Call GpsLocationService to remove the geofence
	    	geofences.removeGeofence(getApplicationContext(), clickedGeofenceMarker.getPosition(), 
	    			clickedGeofenceMarker.getTitle());
	    	clickedGeofenceMarker.remove();
	    	
	    	nullifyGraphics();
    	}
    	else{
    		Toast.makeText(getApplicationContext(),
                    "Valitse poistettava sijainti!", Toast.LENGTH_SHORT).show();
    	}
    }
    
    
    //*************************************
    // Show dialog if gps is not enabled
    //*************************************
    public void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
        .setCancelable(false)
        .setPositiveButton("Goto Settings Page To Enable GPS",
                new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                Intent callGPSSettingIntent = new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    
 
    //*********************************************
    // function to load a map
    //*********************************************
    private void initMap() {
        if (map == null) {
        	map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        	map.setOnInfoWindowClickListener(this);
        	
            // check if map is created successfully or not
            if (map == null) {
                Toast.makeText(getApplicationContext(),
                        "Kartan luominen ep�onnistui", Toast.LENGTH_SHORT).show();
            }
            else{
                // Map settings
                map.setMyLocationEnabled(true); 		//users current location
                map.getUiSettings().setCompassEnabled(true);		//compass
                map.getUiSettings().setMyLocationButtonEnabled(true);	//mylocation button
                
                setMapListeners();
                
            	createMarkers();
            }
        }
    }
    
    
    //***************************************
    // Create markers on a map
    //*************************************
    private void createMarkers(){
    	if(geofenceList != null){
    		for (Geofence curGeof : geofenceList) {		//iterate through the locationList
        		MarkerOptions markerOptions = new MarkerOptions()
			     	.position(curGeof.getLatLng())
			     	.title(curGeof.getPlace())
			     	.snippet(curGeof.getActivity() +", "+ curGeof.getRadius() + " m");
        		map.addMarker(markerOptions);
    		}
    	}
    }
    
 
    @Override
    protected void onResume() {
        super.onResume();
        if(!mapFirstCreate){
        	initMap();
        }
    }
    
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    }


	@Override
	public void onInfoWindowClick(Marker arg0) {
		
		
	}
        
}

