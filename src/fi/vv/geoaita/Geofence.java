// Luokka geofencejen tietojen säilömiseksi
// Juho Uusi-Luomalahti		21.8.2014

package fi.vv.geoaita;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Geofence {
	private LatLng centerLatLng;
	private String activity, place;
	private int rad;
	
	
	//********************
	// Constructor
	//********************
	public Geofence(LatLng centerLatLng, String activity, String place, int rad){
		this.centerLatLng = centerLatLng;
		this.activity = activity;
		this.place = place;
		this.rad = rad;
	}
	
	public int getRadius(){
		return rad;
	}
	
	public LatLng getLatLng(){
		return centerLatLng;
	}
	
	public String getActivity(){
		return activity;
	}
	
	public String getPlace(){
		return place;
	}
}
