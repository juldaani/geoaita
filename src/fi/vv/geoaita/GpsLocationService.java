// Service gps-tiedon tarjoamiseksi muille sovelluksen osille
// Juho Uusi-Luomalahti		21.8.2014

package fi.vv.geoaita;

import java.util.LinkedList;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;


public class GpsLocationService extends Service {
    // Binder given to clients for bounding to the service
    private final IBinder serviceBinder = new LocalBinder();
    
    private LocationManager locationManager;
    //private LocationListener locationListener;
    
    private boolean isGpsEnabled;
    
   // private AnalyzedData analyzedLocData;
    
    GeofenceReaderWriter geofences;
    
    @Override
	public void onCreate(){
        final ProcessLocations ProcessLocations = new ProcessLocations();
        
        isGpsEnabled = false;
        
        geofences = new GeofenceReaderWriter(getApplicationContext());
        
        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        
        //	Check is gps enabled
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            isGpsEnabled = true;
        }
        
        Runnable runnable = new Runnable() {
        	private LocationManager locManager;
            private LocationListener locListener; 
            private AnalyzedData analyzedLocData;
            private int prevLocUpdateInterval = 0, curLocUpdateInterval = 0, locUpdateInterval;
            double dist2nearest, locFixSpeed, time2nearest;
        	
	        public void run() {
	        	Looper.prepare();
	        	
	        	if(locManager == null){
	        		locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	        	}
	        	
	        	while(true){
	        	
	        	//Define a listener that responds to location updates
	        	locListener = new LocationListener() {
	            	public void onLocationChanged(Location location) {
	            		analyzedLocData = ProcessLocations.inputLocation(location, geofences.getGeofenceList() );
	            		
	            		if(analyzedLocData != null){
		            		dist2nearest = analyzedLocData.getDist2nearest();	// [m]
		    	        	locFixSpeed = analyzedLocData.getLocFixSpeed();		// [m/s]
		    	        	
		    	        	// If moving slower than walking speed then use walking speed in calculations
		    	        	if( locFixSpeed < 1.5 ){
		    	        		locFixSpeed = 1.5;
		    	        	}
		    	        	
		    	        	//double shortestTime2nearest = dist2nearest / Parameters.MAX_SPEED;		// [s]
		    	        	time2nearest = dist2nearest / locFixSpeed;
		    	        	
	            		}
	                }
	            	
	                public void onStatusChanged(String provider, int status, Bundle extras) {}

	                public void onProviderEnabled(String provider) {}

	                public void onProviderDisabled(String provider) {}
	        	};
	        	
	        	
	        		
        		try {
        		      Thread.sleep(500);
        		      // Do some stuff
        		    } catch (Exception e) {
        		      e.getLocalizedMessage();
        		    }
	        		
	        	locUpdateInterval = Parameters.DEF_UPDATE_INTERVAL;		// [ms]
	        	
	        	// If device is close enough of some geofence, then set faster location update interval
	        	if( (analyzedLocData != null) && (time2nearest < (Parameters.DEF_UPDATE_INTERVAL*1.5)) ){
	        		locUpdateInterval = 1000;
		        }
	        	
	        	prevLocUpdateInterval = curLocUpdateInterval;
	        	curLocUpdateInterval = locUpdateInterval;
	        	
	        	// If update interval has changed then update location manager
	        	if( prevLocUpdateInterval != curLocUpdateInterval ){
	        		locManager.removeUpdates(locListener);
	        		// Register the listener with the Location Manager to receive location updates
		            //locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, locUpdateInterval, 0, locListener);
		            //locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, locUpdateInterval, 0, locListener);
		            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 
		            		0, locListener, null);
	        	}
	            
	        	// Send message to handler
            	if( (analyzedLocData != null) && (analyzedLocData.isInsideGeofence() == true) ){
		        	Message msg = handler.obtainMessage();
	    			Bundle bundle = new Bundle();
	    			bundle.putString("GEOFENCE", analyzedLocData.getGeofenceName());
	    			msg.setData(bundle);
	    			handler.sendMessage(msg);
            	}
	        	}
	        }
      };
      Thread gpsThread = new Thread(runnable);
      gpsThread.start();
		
    }
    
    
	//*************************
    // Handler for gpsThread
    //***************************
    Handler handler = new Handler(Looper.getMainLooper()) {
		  @Override
		  public void handleMessage(Message msg) {
			  Bundle bundle = msg.getData();
			  String geofName = bundle.getString("GEOFENCE");
			  Toast.makeText(getApplicationContext(), geofName, Toast.LENGTH_SHORT).show();
		     }
	    };
    
    
    @Override
    public void onDestroy(){
    	deleteNotification();
    }
    
    
    //*******************************
    // Delete notification from status bar
    //*******************************
    private void deleteNotification(){
		String ns = Context.NOTIFICATION_SERVICE;
	    NotificationManager nMgr = (NotificationManager) this.getSystemService(ns);
	    nMgr.cancel(0);
    }
    
    
    //**************************
    // Create notification 
    //***************************
    /* private void createNotification(){
    	 NotificationCompat.Builder mBuilder =
    			    new NotificationCompat.Builder(this)
    			    .setSmallIcon(R.drawable.ic_launcher)
    			    .setContentTitle("AjoLoggeri")
    			    .setContentText("Lis�� merkint�")
    			    .setPriority(2);
    			    //.setAutoCancel(true);
    	 Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    	 mBuilder.setSound(notificationSound);
    	 
    	 Intent resultIntent = new Intent(this, InputEventActivity.class);
    	 PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent,
    			 PendingIntent.FLAG_UPDATE_CURRENT);
    	 mBuilder.setContentIntent(resultPendingIntent);
    	 NotificationManager mNotificationManager =
    			    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    	 mNotificationManager.notify(0, mBuilder.build());
     }*/
    
    
    // ******************************************
    // Class for defining binding interface for clients
    //********************************************
    public class LocalBinder extends Binder {
    	GpsLocationService getService() {
            // Return this instance of the service so clients can call public methods
            return GpsLocationService.this;
        }
    }
    
	public LinkedList<Geofence> getGeofenceList(){
		return geofences.getGeofenceList();
	}
	    
    //*******************************************
    // Method for clients for binding to the service
    //*******************************************
    @Override
    public IBinder onBind(Intent intent) {
        return serviceBinder;
    }
    
}

