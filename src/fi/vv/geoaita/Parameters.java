// Sovelluksen ohjausparametrit
// Juho Uusi-Luomalahti			21.8.2014

package fi.vv.geoaita;

public class Parameters {
	
	// time [ms] between location updates received from location manager
	protected static final long LOC_UPDATE_INTERVAL = 3000;
	
	// length of filtering window (cannot be greater than PREV_LOCATIONS_LIST_LEN)
	protected static final int FILTER_WINDOW_LEN = 3;
	
	// Length of list of previous locations.
	protected static final int PREV_LOCATIONS_LIST_LEN = 5;
	
	// maximun assumed speed [m/s]
	protected static final int MAX_SPEED = 42;
	
	// default location update interval
	protected static final int DEF_UPDATE_INTERVAL = 180000;	// [ms]
			
	// radius for analyzing is mobile device moving (if enough points are inside the radius then device is still) [m]
	protected static final int STOP_RADIUS = 10;
	
	// number of points which should lie inside the STOP_RADIUS we can conclude that device is still
	// (should be less than PREV_LOCATIONS_LIST_LEN)
	protected static final int POINT_QTY_STOPPED = 7;

	// Dropbox account keys
	protected static final String APP_KEY = "0qvala59l4ly3ig";
	protected static final String APP_SECRET = "0xkiol5pjgpmliq";
	
	// Max cache size in the local file system for cached files (for dropbox syncing) [bytes]
	protected static final int MAX_CACHE = 10000000;
	
	protected static final String PASSWORD ="";
}
