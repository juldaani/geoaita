// Android-sovellus, ajop�iv�kirja kuljetusliikkeelle
// TTY, Porin laitos			21.8.2014

package fi.vv.geoaita;



import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFileSystem;

import fi.tut.pori.ajologgeri.R;
import fi.vv.geoaita.GpsLocationService.LocalBinder;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {
    
	GpsLocationService gpsLocationService;
    private boolean mBound = false;

	// for dropbox
	private boolean isLinked;	
	private DbxAccountManager mDbxAcctMgr;
	private DbxFileSystem dbxFs; 
	private static final int REQUEST_LINK_TO_DBX = 0;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Dropbox definitions
		mDbxAcctMgr = DbxAccountManager.getInstance(getApplicationContext(), 
				Parameters.APP_KEY, Parameters.APP_SECRET);
		isLinked = mDbxAcctMgr.hasLinkedAccount();
		// link to dropbox
		if(!isLinked){
			mDbxAcctMgr.startLink((Activity)this, REQUEST_LINK_TO_DBX);
		}
				
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        //	Check is gps enabled
        if( !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ){
        	showGPSDisabledAlertToUser();
        }
        
    }
    
    
    //*******************************************
    // Show alert box when back button is pressed
    //******************************************
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
        	createShutdownAlertBox();
        }
        return super.onKeyDown(keyCode, event);
    }
    
    
    //******************************************
    // Create shutdown alert box
    //*****************************************
    private void createShutdownAlertBox(){
        AlertDialog.Builder alertbox = new AlertDialog.Builder(MainActivity.this);
        alertbox.setTitle("Haluatko varmasti lopettaa?");
        
        alertbox.setPositiveButton("Kyll�", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) { 
            	finish();
            }
        });

        alertbox.setNegativeButton("Ei", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            	// if clicked no, do nothing
          }
        });

        alertbox.show();
    }
    
    
    //*********************************
    // Callback from dropbox linking method
    //********************************
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == REQUEST_LINK_TO_DBX) {
	        
	    	if (resultCode == Activity.RESULT_OK) {
	        	Toast.makeText(getApplicationContext(), "Linked to Dropbox", 
	            		Toast.LENGTH_LONG).show();
				// Set max cache size. Once set, the setting will remain in effect across app restarts.
	        	try {
					dbxFs = DbxFileSystem.forAccount(mDbxAcctMgr.getLinkedAccount());
					dbxFs.setMaxFileCacheSize(Parameters.MAX_CACHE);
				} catch (DbxException e) {
					e.printStackTrace();
				}
	        	
	        } 
	    	else {
	        	Toast.makeText(getApplicationContext(), "Failed to link Dropbox", 
	            		Toast.LENGTH_LONG).show();
	        }
	    } 
	    else {
	        super.onActivityResult(requestCode, resultCode, data);
	    }
	    
	}
    

    //*************************************
    // Show dialog if gps is not enabled
    //*************************************
    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
        .setCancelable(false)
        .setPositiveButton("Goto Settings Page To Enable GPS",
                new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                Intent callGPSSettingIntent = new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
     
    
    //*******************************************
    // Start logging locations
    //******************************************    
    public void startServiceButtonClick(View view){
        // Bind to GpsLocationService
        Intent intent = new Intent(this, GpsLocationService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        
        // Start animation for target image
        ImageView img = (ImageView)findViewById(R.id.target_img);
        img.setVisibility(View.VISIBLE);
        img.setBackgroundResource(R.drawable.blink_anim);
        AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();
        frameAnimation.start();
	}
    
    
    //**********************************************
    // Stop logging button click
    //**********************************************    
    public void stopServiceButtonClick(View view){
    	createShutdownAlertBox();
    }
    
    
    //*************************************************
    // Open MapActivity to edit geofences
    // (password needed)
    // *************************************************
    public void setOnMapButtonClick(View view){
    	
    	AlertDialog.Builder alert = new AlertDialog.Builder(this);

    	alert.setTitle("Salasana");
    	alert.setMessage("Sy�t� salasana");

    	// Password input
    	final EditText password = new EditText(this);
    	alert.setView(password);

    	// Listeners
    	alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	    	public void onClick(DialogInterface dialog, int whichButton) {
	    		String pass = password.getText().toString();
	    		
	    		if(pass.equals(Parameters.PASSWORD) ){
	    	    	// Start activity
	    	    	Intent i = new Intent(getBaseContext(), MapActivity.class);
	    	        startActivity(i);
	    		}
    	  	}
    	});
    	alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int whichButton) {
    	    // Do nothing if canceled
    		}
    	});

    	alert.show();
    }
    
    
    //*************************************************
    // Callback for binding/unbinding to the service
    // *************************************************
    private ServiceConnection mConnection = new ServiceConnection() {

    	// When bound to the service
        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LocalBinder binder = (LocalBinder) service;
            gpsLocationService = binder.getService();
            mBound = true;

            Toast.makeText(getApplicationContext(), "Paikannus k�ynnistetty", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            Toast.makeText(getApplicationContext(), "Paikannus pys�ytetty", Toast.LENGTH_SHORT).show();
        }
    };

    
    @Override
    public void onStart() {
        super.onStart();
        
    }

    
    @Override
    protected void onPause() {
        super.onPause();

    }

    
    @Override
    protected void onResume() {
        super.onResume();

    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    	// Unbind the service
    	if (mBound) {
	    	unbindService(mConnection);
	        mBound = false;
	        Toast.makeText(getApplicationContext(), "Paikannus pys�ytetty", Toast.LENGTH_SHORT).show();
    	}
    }
   
}
